import {useState} from "react";
import './App.css';
import {Form} from "./Form";
import {initialStateArr} from "./service/initialState";
import {User} from "./User";
import {Comment} from "./Comment";


export const App = () => {

    const [data, setData] = useState([...initialStateArr])
    const [input, setInput] = useState([...data])
    const [comment, setComment] = useState([])
    const [number, setNumber] = useState([])


    let newInput = input.filter(value => Object.keys(value).length !== {});

    console.log(input)
    console.log(newInput)

    const addComment = (e) => {
        e.preventDefault()

        const newComment = {
            name: e.target[0].value,
            description: e.target[1].value,
            id: e.target[2].value
        };
        setComment((data) => [...data, newComment]);
        e.target.reset();
        // console.
    }
    const addUser = (e) => {
        e.preventDefault()

        const newUser = {
            name: e.target[0].value,
            description: e.target[1].value,
            id: e.target[2].value
        };
        setInput((data) => [...data, newUser]);
        e.target.reset();


    }
    const addNumber = (e) => {
        e.preventDefault()

        const newNumber = {
            name: e.target[0].value,
            description: e.target[1].value,
            id: e.target[2].value
        };
        setInput((data) => [...data, newNumber]);
        e.target.reset();


    }
    return (
        <div className="App">

            <div>
                {newInput.map((item, i) => (
                    <User key={item.name + i} {...item} />
                ))}

            </div>


            <div>
                <Form onSubmit={addUser}
                      buttonText={"add user"}

                />

                <Form onSubmit={addComment}
                      buttonText={" add comment"}

                />
                <Form onSubmit={addNumber}
                      buttonText={" add Number"}

                />
            </div>

        </div>


    );
}

