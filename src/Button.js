import React from 'react';
import "./index.css"

export const Button = (props) => {
    return (
        <button className={"aria" + (props.className || "")}>
            {props.children}
        </button>
    )
}