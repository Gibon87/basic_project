import React from 'react'
import {Button} from "./Button";


export const User = ({id, name, description, comment}) => {
    let addComment = "dodaj komentarz";

    return (
        <>
            <h2> {name} {description} {id}</h2>
            <Button>{addComment}</Button>
            <p>{comment}</p>
        </>
    )
}
