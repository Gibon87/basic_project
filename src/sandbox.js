import {useState} from "react";
import './App.css';
import {Form} from "./Form";
import {initialStateArr} from "./service/initialState";
import {User} from "./User";
import {Comment} from "./Comment";


export const App = () => {

    const [data, setData] = useState([...initialStateArr])
    const [input, setInput] = useState([...data])
    const [comment, setComment] = useState([])

    let newInput =input.filter(
        obj => !(obj && Object.keys(obj).length === 0)
    );
    console.log(newInput)
    // console.log(input)

    const addComment = (e) => {
        e.preventDefault()

        const newComment = {
            name: e.target[0].value,
            description: e.target[1].value,
            id: e.target[2].value
        };
        setComment((data) => [newComment]);
        e.target.reset();
        // console.
    }

    // to do
    // prevent
    // add empty value from input to array
    const addUser = (e) => {
        e.preventDefault()

        const newUser = {
            name: e.target[0].value,
            description: e.target[1].value,
            id: e.target[2].value
        };
       if (newUser !== {}) {
            setInput((data) => [...data, newUser])
        } else {
           console.log("aa")
       }
        e.target.reset();

        const Comment = () => {
            {comment.map((item, i) => (

                <Comment key={item.name + i} {...item} />
            ))}
        }
    }

    return (
        <div className="App">
            {newInput.map((item, i) => (
                <User key={item.name + i} {...item} />
            ))}


            <div><Form onSubmit={addUser}
                       buttonText={"add user"}

            />

                <Form onSubmit={addComment}
                      buttonText={" add comment"}

                />
            </div>

        </div>


    );
}

